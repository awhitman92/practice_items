def get_n(n, start=1):
    """
    :param n: The index of the Fibonacci value to return. This number starts at 0.
    :param start: Indicate if the first value is a 1 or 0. Default is 1.
    :return: The Fibonacci value at index n
    """
    if start != 0:
        start = 1
    previous_previous_number = start
    previous_number = 1
    current_number = 0
    search_list = []
    if n == 0:
        return start, [start]
    elif n == 1:
        return 1, [start, 1]
    for index in range(n):
        # first number as defined
        if index == 0:
            current_number = previous_previous_number
        # second number as defined
        elif index == 1:
            current_number = previous_number
        # all other numbers are the sum of the previous two numbers
        else:
            current_number = previous_number + previous_previous_number
            previous_previous_number = previous_number
            previous_number = current_number
        search_list.append(current_number)
    return current_number, search_list
