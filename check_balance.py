import re


def is_balanced(input):
    """
    :param input: string that may or may not be balanced
    :return: True if string is balanced, False if it is not balanced.
    """
    regex_string = '''[^(\[|\]|{|}|(|))]'''
    input = re.sub(regex_string, '', input)
    if len(input) < 2:
        return False
    previous_input = "0"
    while input != previous_input:
        regex_string = '''(\[\]|{}|\(\))'''
        previous_input = input
        input = re.sub(regex_string, '', input)
    if len(input) != 0:
        return False
    else:
        return True
