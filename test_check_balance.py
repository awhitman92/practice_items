from check_balance import is_balanced

def test_invalid_inputs():
    inputs = [
        "test",
        "[test]]",
        "[{()}]{",
        "{{{[][][}](){]",
        "[()]{}{[()()]()}{}{{P{}]]]}[)",
        "[(])"
        "{"
    ]
    for each_input in inputs:
        assert is_balanced(each_input) is False


def test_valid_inputs():
    inputs = [
        "{[{(test)}]}",
        "[test]",
        "{}[]()",
        "{}",
        "[{()}]",
        "{{{[][]}}}",
        "[()]{}{[()()]()}{}",
        "[()[]]{{{}}}"
    ]
    for each_input in inputs:
        assert is_balanced(each_input) is True
