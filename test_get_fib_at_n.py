from get_fib_at_n import get_n



def run_test(test_values):
    try:
        if test_values["start"] is None:
            result = get_n(test_values["n"])
        else:
            result = get_n(test_values["n"], test_values["start"])
        assert result[0] == test_values["expected_result"]
    except KeyError:
        return None

def test_run_all_tests():
    test_values = [
        {
            "n": 1,
            "start": None,
            "expected_result": 1
        },
        {
            "n": 5,
            "start": None,
            "expected_result": 5
        },
        {
            "n": 6,
            "start": None,
            "expected_result": 8
        },
        {
            "n": 0,
            "start": 0,
            "expected_result": 0
        },
        {
            "n": 1,
            "start": 0,
            "expected_result": 1
        },
        {
            "n": 45,
            "start": 1,
            "expected_result": 1134903170
        },
        {
            "n": 44,
            "start": 1,
            "expected_result": 701408733
        },
        {
            "n": 43,
            "start": 1,
            "expected_result": 433494437
        },
        {
            "n": 42,
            "start": 1,
            "expected_result": 267914296
        },
        {
            "n": 10,
            "start": 1,
            "expected_result": 55
        },
        {
            "n": 10,
            "start": 11,
            "expected_result": 55
        }
    ]
    for each_test in test_values:
        run_test(each_test)

